#!/usr/bin/python3
import sys
sys.path.append('lib')
import cndprint  # noqa: E402
import cnd_io  # noqa: E402
import yaml  # noqa: E402
import cnd_scaffold  # noqa: E402
from consul_scaffold import ConsulScaffold 

_consul_scaffold = ConsulScaffold()
_consul_scaffold.step('init')