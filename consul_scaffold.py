#!/usr/bin/python3
import sys
sys.path.append('lib')
import cndprint  # noqa: E402
import cnd_io  # noqa: E402
import yaml  # noqa: E402
import cnd_scaffold  # noqa: E402


_source = {
	'project_id': 'model/gitopsmodel',
	'definition': 'consul-acl',
	'branch': 'main',
}
_target = {
	'project_id': 'build',
	'folder': 'tfe',
	'branch': 'main',
}
level = "Trace"
_print = cndprint.CndPrint(level=level, silent_mode=False)
_provider = cnd_io.CndProviderLocalfile(creds={}, print=_print)
_cnd_io = cnd_io.CndIO(_provider, print=_print)

class ConsulScaffold():
	def __init__(self):
		self._source = _source
		self._target = _target
		self._cnd_io = _cnd_io
		self._print = _print
		self.data_to_replace = {}

	def _build_data(self):
		self._build_acl_org()
		self._build_acl_org_product()
		
	def step(self, step, data=None):
		if data is None:
			self._build_data()
		else:
			self.data_to_replace = data
		scaffold = cnd_scaffold.CndScaffold(self._source, self._target, self.data_to_replace, self._cnd_io, print=self._print, engine='jinja2')
		scaffold._validate_structure()
		scaffold._load_files(step)
		scaffold._push_files()
		
	def _build_acl_org(self):
		for action in ['read', 'write']:
			content_raw = self._cnd_io.pull_file('data/ConsulDemo', f'acl-org-{action}.yml')
			content = yaml.safe_load(content_raw)
			yaml_result = []
			for org in content:
				yaml_result.append({'org': org})
			self.data_to_replace[f"org_{action}"] = yaml_result
			
	def _build_acl_org_product(self):
		for action in ['read', 'write']:
			content_raw = self._cnd_io.pull_file('data/ConsulDemo', f'acl-product-{action}.yml')
			content = yaml.safe_load(content_raw)
			yaml_result = []
			for org in content:
				for product in content[org]:
					yaml_result.append({'org': org, 'product': product})
			self.data_to_replace[f"product_{action}"] = yaml_result			