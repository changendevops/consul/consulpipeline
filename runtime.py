#!/usr/bin/python3
import sys
import os
sys.path.append('lib')
import cndprint  # noqa: E402
import cnd_io  # noqa: E402
import yaml  # noqa: E402
import cnd_scaffold  # noqa: E402
from consul_scaffold import ConsulScaffold 


_consul_scaffold = ConsulScaffold()
_consul_scaffold.step('runtime', data={
	'host': 'localhost:8500', 
	'scheme': 'http', 
	'path': 'terraform/cnd/consul/state', 
	'access_token': os.environ['ACCESS_TOKEN'], 
	'token': os.environ['TOKEN']
})