#!/usr/bin/python3
import sys
sys.path.append('lib')
import cndprint  # noqa: E402
import cnd_io  # noqa: E402
import yaml  # noqa: E402
import cnd_scaffold  # noqa: E402
from consul_scaffold import ConsulScaffold 

_consul_scaffold = ConsulScaffold()
_consul_scaffold.step('init')

# build_tfe = BuildTFE()
# build_tfe.step('runtime', data={
# 	'host': 'localhost:8500', 
# 	'scheme': 'http', 
# 	'path': 'terraform/cnd/consul/state', 
# 	'access_token': '4fe12c67-8a9f-0188-f613-c4f8471c439e', 
# 	'token': 'a278e239-35eb-e736-59d6-7858b42f02c6'
# })